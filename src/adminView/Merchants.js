import React from "react";
import {
  List,
  Datagrid,
  ReferenceField,
  TextField,
  DateField,
  EmailField,
  UrlField,
  NumberField,
  Filter,
  ReferenceManyField,
  TextInput,
  AutocompleteInput,
  EditButton,
  DeleteButton,
  ArrayField,
  ArrayInput,
  NullLabelTextInput,
  BooleanField,
  Pagination,
  SimpleShowLayout,
  Show,
  perPage,
} from "react-admin";
import {
  Create,
  Edit,
  SimpleForm,
  SimpleFormIterator,
  ReferenceInput,
  BooleanInput,
  DisabledInput,
  EmailInput,
  PasswordInput,
  LongTextInput,
  NumberInput,
  SelectInput,
  ListGuesser,
} from "react-admin";
// const choicesCategory = ["Sport", ""];
const EditTitle = ({ record }) => {
  return <span>Update {record ? `"${record.name}"` : ""}</span>;
};
const CreateTitle = ({ record }) => {
  return <span>Create {record ? `"${record.name}"` : ""}</span>;
};
const MerchantFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Search by Enterprise" source="enterprise" alwaysOn />
  </Filter>
);

export const MerchantList = (props) => (
  <List {...props} filters={<MerchantFilter />}>
    <Datagrid rowClick="show">
      <BooleanField source="foodShop" />
      <TextField source="category" />
      <TextField source="enterprise" />
      <TextField source="name" />
      <TextField source="phone" />
      <TextField source="email" />
      <TextField source="address.number" />
      <TextField source="address.street" />
      <TextField source="address.zipcode" />
      <TextField source="address.city" />
      <EditButton />
      <DeleteButton />
    </Datagrid>
  </List>
);
export const MerchantShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <BooleanField source="foodShop" />
      <TextField source="category" />
      <TextField source="enterprise" />
      <TextField source="name" />
      <TextField source="phone" />
      <TextField source="email" />
      <TextField source="address.number" />
      <TextField source="address.street" />
      <TextField source="address.zipcode" />
      <TextField source="address.city" />
      <ArrayField source="products">
        <Datagrid>
          <TextField source="name" />
          <TextField source="price" />
          <TextField source="picture" />
          <BooleanField source="available" />
        </Datagrid>
      </ArrayField>
    </SimpleShowLayout>
  </Show>
);

export const MerchantEdit = (props) => (
  <Edit {...props} title={<EditTitle />}>
    <SimpleForm>
      <BooleanInput
        source="foodShop"
        valueLabelTrue="Alimentaire"
        valueLabelFalse="Non alimentaire"
      />
      <TextInput source="category" resettable />
      <TextInput source="enterprise" resettable />
      <TextInput source="name" resettable />
      <TextInput source="phone" resettable />
      <TextInput
        type="email"
        source="email"
        placeholder="example@gmail.it"
        resettable
      />
      {/* <TextInput type="password" source="password" resettable /> */}
      <TextInput source="address.number" label="address Number" resettable />
      <TextInput source="address.street" label="address Street" resettable />
      <TextInput source="address.zipcode" label="address Zipcode" resettable />
      <TextInput source="address.city" label="address City" resettable />
      <TextInput source="IBAN" resettable />
      <TextInput source="siretNumber" resettable />
      <BooleanInput source="isOpen" />
      <ArrayInput source="products" label="">
        <SimpleFormIterator>
          <TextInput source="name" resettable />
          <TextInput source="price" resettable />
          <TextInput source="picture" resettable />
          <BooleanInput source="available" />
        </SimpleFormIterator>
      </ArrayInput>
    </SimpleForm>
  </Edit>
);
export const MerchantCreate = (props) => (
  <Create {...props} component="div" title={<CreateTitle />}>
    <SimpleForm>
      <BooleanInput source="foodShop" defaultValue />
      <TextInput source="category" resettable />
      <TextInput source="enterprise" />
      <TextInput source="name" resettable />
      <TextInput source="phone" resettable />
      <TextInput
        type="email"
        source="email"
        placeholder="example@gmail.it"
        resettable
      />
      <TextInput type="password" source="password" resettable />
      <TextInput source="address.number" label="address Number" resettable />
      <TextInput source="address.street" label="address Street" resettable />
      <TextInput source="address.zipcode" label="address Zipcode" resettable />
      <TextInput source="address.city" label="address City" resettable />
      <TextInput source="IBAN" resettable />
      <TextInput source="siretNumber" resettable />
      <BooleanInput source="isOpen" />
      <ArrayInput source="products" label="">
        <SimpleFormIterator>
          <TextInput source="name" resettable />
          <TextInput source="price" resettable />
          <TextInput source="picture" resettable />
          <BooleanInput source="available" />
        </SimpleFormIterator>
      </ArrayInput>
    </SimpleForm>
  </Create>
);

// export const PostCreate = props => (
//     <Create {...props}>
//         <SimpleForm>
//             <ReferenceInput source="userId" reference="users">
//                 <SelectInput optionText="name" />
//             </ReferenceInput>
//             <TextInput source="title" />
//             <LongTextInput source="body" />
//         </SimpleForm>
//     </Create>
// );
