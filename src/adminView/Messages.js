import React from "react";
import {
  List,
  Datagrid,
  TextField,
  DateField,
  EmailField,
  UrlField,
  NumberField,
  Filter,
  TextInput,
  ArrayField,
  EditButton,
  DeleteButton,
  Show,
  SimpleShowLayout,
} from "react-admin";

export const MessageList = (props) => (
  <List {...props}>
    <Datagrid rowClick="show">
      <TextField source="idOrder" />
      <DateField source="createdAt" />
      <ArrayField source="messageDeliverer">
        <Datagrid>
          <TextField source="state" />
          <TextField source="idDeliverer" />
          <TextField source="text" />
          <DateField source="date" showTime />
        </Datagrid>
      </ArrayField>
      <ArrayField source="messageCustomer">
        <Datagrid>
          <TextField source="state" />
          <TextField source="idCustomer" />
          <TextField source="text" />
          <DateField source="date" showTime />
        </Datagrid>
      </ArrayField>
      {/* <EditButton />
      <DeleteButton /> */}
    </Datagrid>
  </List>
);

export const MessageShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="idOrder" />
      <DateField source="createdAt" />
      <ArrayField source="messageDeliverer">
        <Datagrid>
          <TextField source="state" />
          <TextField source="idDeliverer" />
          <TextField source="text" />
          <DateField source="date" showTime />
        </Datagrid>
      </ArrayField>
      <ArrayField source="messageCustomer">
        <Datagrid>
          <TextField source="state" />
          <TextField source="idCustomer" />
          <TextField source="text" />
          <DateField source="date" showTime />
        </Datagrid>
      </ArrayField>
      {/* <EditButton />
      <DeleteButton /> */}
    </SimpleShowLayout>
  </Show>
);
