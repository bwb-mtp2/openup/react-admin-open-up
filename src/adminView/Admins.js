import React from "react";
import {
  List,
  Datagrid,
  TextField,
  DateField,
  EmailField,
  EmailInput,
  UrlField,
  NumberField,
  Filter,
  TextInput,
  ArrayField,
  EditButton,
  DeleteButton,
  Show,
  SimpleShowLayout,
  Create,
  Edit,
  SimpleForm,
} from "react-admin";

const ListTitle = ({ record }) => {
  return <span>Show {record ? `"${record.username}"` : ""}</span>;
};

export const AdminList = (props) => (
  <List {...props}>
    <Datagrid rowClick="show">
      <TextField source="firstName" />
      <TextField source="lastName" />
      <TextField source="username" />
      <TextField source="phone" />
      <EmailField source="email" type="email" />
      <EditButton />
    </Datagrid>
  </List>
);

export const AdminShow = (props) => (
  <Show {...props} title={<ListTitle />}>
    <SimpleShowLayout>
      <TextField source="firstName" />
      <TextField source="lastName" />
      <TextField source="username" />
      <TextField source="phone" />
      <EmailField source="email" type="email" />
      {/* <TextField source="password" type="password" /> */}
      <EditButton />
      {/* <DeleteButton /> */}
    </SimpleShowLayout>
  </Show>
);

export const AdminCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="firstName" />
      <TextInput source="lastName" />
      <TextInput source="username" />
      <TextInput source="phone" />
      <TextInput source="email" type="email" />
      <TextInput source="password" type="password" />
      {/* <DeleteButton /> */}
    </SimpleForm>
  </Create>
);

export const AdminEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source="firstName" />
      <TextInput source="lastName" />
      <TextInput source="username" />
      <TextInput source="phone" />
      <TextInput source="email" type="email" />
      <TextInput source="password" type="password" />
      {/* <DeleteButton /> */}
    </SimpleForm>
  </Edit>
);
