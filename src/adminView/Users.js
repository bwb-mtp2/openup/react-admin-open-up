import React, { useState } from "react";
import {
  List,
  Datagrid,
  TextField,
  DateField,
  EmailField,
  UrlField,
  NumberInput,
  NumberField,
  Filter,
  TextInput,
  SimpleForm,
  Create,
  Edit,
  BooleanInput,
  ArrayInput,
  SimpleFormIterator,
  Show,
  SimpleShowLayout,
  EditButton,
  DeleteButton,
  BooleanField,
  ArrayField,
} from "react-admin";

const UserFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Search by name" source="lastName" alwaysOn />
  </Filter>
);

const EditTitle = ({ record }) => {
  return <span>Update {record ? `"${record.firstName}"` : ""}</span>;
};

export const UserList = (props) => (
  <List {...props} filters={<UserFilter />}>
    <Datagrid rowClick="show">
      <BooleanField source="deliverer" />
      <TextField source="firstName" />
      <TextField source="lastName" />
      <TextField source="address.number" />
      <TextField source="address.street" />
      <TextField source="address.zipcode" />
      <TextField source="address.city" />
      <TextField source="phone" />
      <EmailField source="email" />
      <NumberField source="userPoint" />
      <NumberField source="like" />
      <EditButton />
      <DeleteButton />
    </Datagrid>
  </List>
);

export const UserShow = (props) => (
  <Show {...props} filters={<UserFilter />}>
    <SimpleShowLayout>
      <BooleanField source="deliverer" />
      <TextField source="firstName" />
      <TextField source="lastName" />
      <TextField source="address.number" />
      <TextField source="address.street" />
      <TextField source="address.zipcode" />
      <TextField source="address.city" />
      <TextField source="phone" />
      <EmailField source="email" />
      <NumberField source="userPoint" />
      <NumberField source="like" />
      <ArrayField source="deliveryHours">
        <Datagrid>
          <TextField source="start" />
          <TextField source="end" />
        </Datagrid>
      </ArrayField>
      <TextField source="deliveryArea.coordinates" />
    </SimpleShowLayout>
  </Show>
);

export const UserCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <BooleanInput source="deliverer" defaultValue />
      <TextInput source="firstName" />
      <TextInput source="lastName" />
      <TextInput source="address.number" />
      <TextInput source="address.street" />
      <TextInput source="address.zipcode" />
      <TextInput source="address.city" />
      <TextInput source="phone" />
      <TextInput source="email" type="email" />
      <TextInput source="password" type="password" />
      <TextInput source="profilPicture" />
      <TextInput source="idCardPicture" />
      <TextInput source="creditCard" />
      <NumberInput source="userPoint" />
      <ArrayInput source="deliveryHours">
        <SimpleFormIterator>
          <TextInput source="start" />
          <TextInput source="end" />
        </SimpleFormIterator>
      </ArrayInput>
      <TextInput source="deliveryArea.coordinates" placeholder="" />
      {/* <ArrayInput source="purchaseHistory">
        <SimpleFormIterator>
          <TextInput source="idOrder" />
        </SimpleFormIterator>
      </ArrayInput>
      <ArrayInput source="deleveryHistory">
        <SimpleFormIterator>
          <TextInput source="idOrder" />
        </SimpleFormIterator>
      </ArrayInput> */}
    </SimpleForm>
  </Create>
);

export const UserEdit = (props) => (
  <Edit {...props} filters={<UserFilter />} title={<EditTitle />}>
    <SimpleForm>
      <BooleanInput source="deliverer" defaultValue />
      <TextInput source="firstName" />
      <TextInput source="lastName" />
      <TextInput source="address.number" />
      <TextInput source="address.street" />
      <TextInput source="address.zipcode" />
      <TextInput source="address.city" />
      <TextInput source="phone" />
      <TextInput source="email" type="email" />
      {/* <TextInput type="password" source="password" resettable /> */}
      <NumberInput source="userPoint" />
      <ArrayInput source="deliveryHours">
        <SimpleFormIterator>
          <TextInput source="start" />
          <TextInput source="end" />
        </SimpleFormIterator>
      </ArrayInput>
      <TextInput source="deliveryArea.coordinates" placeholder="" />
    </SimpleForm>
  </Edit>
);
