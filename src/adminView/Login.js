import React from "react";
import image from "../fd2.jpg";
import { Login, LoginForm } from "react-admin";
import { withStyles } from "@material-ui/core/styles";
const styles = {
  icon: { display: "none" },
};
const CustomLoginForm = withStyles({
  button: { background: "#F15922" },
})(LoginForm);
const CustomLoginPage = (props) => (
  <Login
    // backgroundImage={image}
    backgroundImage="https://source.unsplash.com/random/1600x900/?nature"
    {...props}
  />
);
export default withStyles(styles)(CustomLoginPage);
