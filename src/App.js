// import * as React from "react";
import React, { useEffect } from "react";
import "./App.css";
import {
  Admin,
  Resource,
  ListGuesser,
  EditGuesser,
  ShowGuesser,
  Pagination,
} from "react-admin";
// import UserList from "./UserList";
import {
  MerchantList,
  MerchantEdit,
  MerchantCreate,
  MerchantShow,
} from "./adminView/Merchants";
import { UserList, UserEdit, UserCreate, UserShow } from "./adminView/Users";
import {
  OrderList,
  OrderEdit,
  OrderCreate,
  OrderShow,
} from "./adminView/Orders";
import { MessageList, MessageShow } from "./adminView/Messages";
import {
  AdminList,
  AdminShow,
  AdminEdit,
  AdminCreate,
} from "./adminView/Admins";
import myDataProvider from "./myDataProvider";
import { fetchJson as httpClient } from "./httpClient";
import authProvider from "./authProvider";
import Login from "./adminView/Login";
import { createMuiTheme } from "@material-ui/core/styles";
// import indigo from "@material-ui/core/colors/indigo";
// import pink from "@material-ui/core/colors/pink";
// import red from "@material-ui/core/colors/red";
import orange from "@material-ui/core/colors/deepOrange";
import blue from "@material-ui/core/colors/blue";
import Dashboard from "./adminView/Dashboard";
import IconMerchant from "@material-ui/icons/Store";
import IconAdmin from "@material-ui/icons/Build";
import IconUser from "@material-ui/icons/Person";
import IconOrder from "@material-ui/icons/ShoppingCart";
import IconMessage from "@material-ui/icons/Textsms";
import deepOrange from "@material-ui/core/colors/deepOrange";
import NotFound from "./adminView/NotFound";
// import Menu from "./adminView/Menu";
// import theme from "./adminView/theme";

const theme = createMuiTheme({
  palette: {
    type: "dark", // Switching the dark mode on is a single property value change.
    primary: {
      main: blue[500],
    },
    secondary: {
      main: deepOrange[500],
    },
  },
  typography: {
    fontFamily: "Roboto",
    fontWeightBold: 900,
  },
  shadows: [
    "0px 11px 15px -7px rgba(0,0,0,0.2),0px 24px 38px 3px rgba(0,0,0,0.14),0px 9px 46px 8px rgba(0,0,0,0.12)",
  ],
});

const dataProvider = myDataProvider(
  "http://51.83.69.138:5000/api/admin",
  httpClient
);
const App = () => {
  return (
    <Admin
      theme={theme}
      catchAll={NotFound}
      title="My custom admin"
      loginPage={Login}
      dataProvider={dataProvider}
      authProvider={authProvider}
      dashboard={Dashboard}
    >
      <Resource
        name="merchants"
        list={MerchantList}
        create={MerchantCreate}
        edit={MerchantEdit}
        show={MerchantShow}
        icon={IconMerchant}
      />
      <Resource
        name="users"
        list={UserList}
        create={UserCreate}
        edit={UserEdit}
        show={UserShow}
        icon={IconUser}
      />
      <Resource
        name="orders"
        list={OrderList}
        create={OrderCreate}
        edit={OrderEdit}
        show={OrderShow}
        icon={IconOrder}
      />
      <Resource
        name="messages"
        list={MessageList}
        show={MessageShow}
        icon={IconMessage}
      />
      <Resource
        name="admins"
        list={AdminList}
        show={AdminShow}
        edit={AdminEdit}
        create={AdminCreate}
        icon={IconAdmin}
      />
    </Admin>
  );
};
export default App;
